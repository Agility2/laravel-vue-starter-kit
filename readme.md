# Laravel 5.7 Vue Starter Kit

This is the repo for a starter kit for DesignStudio Projects using Laravel

## List of Technology used.

* Laravel 5.7
* Laravel Passport AUTH
* Vue 2.5.x
* Vuex
* SASS
* Vuetify
* AnimationOnScroll
* JavaScript
* Maria DB
* REST API

## Instructions for how to develop, use, and test the code.

Coming Soon
