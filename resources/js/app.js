require('./bootstrap');
 window.Vue = require('vue');

 
 import Vuetify from 'vuetify';
 import AOS from 'aos';

 // Route information from Vue Router
import { router } from './routes.js';
import { store } from './vuex/store';

Vue.use(Vuetify, {
   theme: {
      primary: '#13252C',
   }
});
Vue.component('navbar-component', require('./components/layout/navbar.vue').default);
Vue.component('sidebar-component', require('./components/layout/sidebar.vue').default);
Vue.component('footer-component', require('./components/layout/footer.vue').default);
Vue.component('passport-clients', require('./components/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

const app = new Vue({
   el: '#app',
   created () {  
      if (this.$store.getters.isAuthenticated) {
         this.$store.dispatch('userRequest');
     }
    AOS.init();
    },
    router,
    store,
});